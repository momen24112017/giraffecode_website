import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { ContactUsComponent } from './contact-us/contact-us.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {
    path:'',
    component:IndexComponent,
    
    },
     {
      path:'contact-us',
      component:ContactUsComponent,
      
      }  

  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
